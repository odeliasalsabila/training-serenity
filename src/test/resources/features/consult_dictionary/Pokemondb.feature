Feature: Get data from pokemon

  Scenario Outline: user getting data pokemon
    Given user open url pokemondb
    When user find Bulbasaur pokemon <name>
    And user click Bulbasaur
    Then user should see the data of pokemon Bulbasaur
      | name | <name> |
      | no   | <no>   |
#      | species | <species> |
#      | height  | <height>  |
#      | weight  | <weight>  |

    Examples:
#      | name      | no  | species      | height        | weight            |
#      | Bulbasaur | 001 | Seed Pokémon | 0.7 m (2′04″) | 6.9 kg (15.2 lbs) |
      | name      | no  |
      | Bulbasaur | 001 |