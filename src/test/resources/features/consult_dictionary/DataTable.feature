Feature: Filling Form

  Scenario Outline: User without hosting and user from florida
    Given open form selenium easy
    When user filling the following data
      | firstName   | <firstName>   |
      | lastName    | <lastName>    |
      | email       | <email>       |
      | phone       | <phone>       |
      | address     | <address>     |
      | city        | <city>        |
      | state       | <state>       |
      | zip         | <zip>         |
      | domain      | <domain>      |
      | hosting     | <hosting>     |
      | description | <description> |
    Then user should see filled form

    Examples:
      | firstName | lastName  | email            | phone        | address   | city     | state      | zip   | domain          | hosting | description |
      | odelia    | salsabila | odelia@gmail.com | 089508775103 | jagiran 1 | surabaya | Alaska     | 60136 | odelia.site.com | true    | lalalal     |
      | odelia    | salsabila | odelia@gmail.com | 089508775103 | jagiran 1 | surabaya | California | 60136 | odelia.site.com | false   | lalalal     |
      | odelia    | salsabila | odelia@gmail.com | 089508775103 | jagiran 1 | surabaya | Alaska     | 60136 | odelia.site.com | true    | lalalal     |
      | odelia    | salsabila | odelia@gmail.com | 089508775103 | jagiran 1 | surabaya | California | 60136 | odelia.site.com | true    | lalalal     |