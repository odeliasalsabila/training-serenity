Feature: Get data from pokemon

  Scenario Outline: user getting data pokemon
    Given user open url serebiinet
    When user find <name> pokemon in serebii
    Then user should see the data of pokemon Bulbasaur in Serebii
      | name | <name> |
      | no   | <no>   |
    Examples:
      | name | no |
      | Bulbasaur | #001 |