package com.training.future.batchfour;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features="src/test/resources/features/consult_dictionary/Pokemondb.feature"
)
public class DefinitionTestSuite {}
