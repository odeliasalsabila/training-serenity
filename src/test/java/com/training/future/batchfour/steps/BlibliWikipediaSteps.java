package com.training.future.batchfour.steps;

import com.openhtmltopdf.css.parser.property.PrimitivePropertyBuilders;
import com.training.future.batchfour.pages.Duckduckgopage;
import com.training.future.batchfour.pages.WikipediaPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.pages.PageObject;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;

public class BlibliWikipediaSteps {
    private Duckduckgopage duckduckgopage;
    private WikipediaPage wikipediaPage;

    @Given("^user open duckduckgo homepage$")
    public void userOpenDuckduckgoHomepage() {
        duckduckgopage.open();
    }

    @When("^user type \"([^\"]*)\" in serach box$")
    public void userTypeInSerachBox(String keyword) {
        duckduckgopage.searching(keyword);
    }

    @And("^user click the link of wikipedia$")
    public void userClickTheLinkOfWikipedia() {
        duckduckgopage.chooseBlibliWikipedia();
    }

    @Then("^user should able to see \"([^\"]*)\"$")
    public void userShouldAbleToSee(String expected){
        MatcherAssert.assertThat("wikipedia is not opened",
                wikipediaPage.getTxtWikipedia(),
                Matchers.containsString(expected));
    }
}
