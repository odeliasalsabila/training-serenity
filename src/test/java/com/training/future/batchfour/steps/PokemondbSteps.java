package com.training.future.batchfour.steps;

import com.training.future.batchfour.pages.PokemondbPage;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.xml.crypto.Data;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class PokemondbSteps {
    private PokemondbPage pokemondbPage;

    @Given("^user open url pokemondb$")
    public void userOpenUrlPokemondb() {
        pokemondbPage.open();
    }

    @When("^user find Bulbasaur pokemon <name>$")
    public void userFindBulbasaurPokemonName(DataTable formDataTable) {
        Map<String, Object> formData = formDataTable.asMap(String.class, Object.class);
        pokemondbPage.clickPokemon();
    }

    @Then("^user should see the data of pokemon Bulbasaur$")
    public void userShouldSeeTheDataOfPokemonBulbasaur(DataTable formDataTable) {
        Map<String, Object> formData = formDataTable.asMap(String.class, Object.class);
        String nameExpected = formData.get("name").toString();
        String noExpected = formData.get("no").toString();

        String nameActual = pokemondbPage.getNamePokemon().getText();
        String noActual = pokemondbPage.getNoPokemon().getText();

        assertThat("name invalid", nameActual, equalTo(nameExpected));
        assertThat("no invalid", noActual, equalTo(noExpected));
    }
}
