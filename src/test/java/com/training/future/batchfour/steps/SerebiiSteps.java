package com.training.future.batchfour.steps;

import com.training.future.batchfour.pages.SerebiiPage;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class SerebiiSteps {
    private SerebiiPage serebiiPage;

    @Given("^user open url serebiinet$")
    public void userOpenUrlSerebiinet() {
        serebiiPage.open();
    }

    @When("^user find <name> pokemon in serebii$")
    public void userFindNamePokemonInSerebii(String name) {
        serebiiPage.findPokemon(name);
    }

    @Then("^user should see the data of pokemon Bulbasaur in Serebii$")
    public void userShouldSeeTheDataOfPokemonBulbasaurInSerebii(DataTable formDataTable) {
        Map<String, Object> formData = formDataTable.asMap(String.class, Object.class);
        String nameExpected = formData.get("name").toString();
        String noExpected = formData.get("no").toString();

        String nameActual = serebiiPage.getPokemonName(nameExpected);
        String noActual = serebiiPage.getPokemonNo(noExpected);

        assertThat("name invalid", nameActual, equalTo(nameExpected));
        assertThat("no invalid", noActual, equalTo(noExpected));
    }
}
