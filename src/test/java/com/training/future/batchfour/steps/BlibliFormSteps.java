package com.training.future.batchfour.steps;

import com.training.future.batchfour.pages.SeleniumPage;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.DefaultUrl;
import org.hamcrest.MatcherAssert;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class BlibliFormSteps {
    private SeleniumPage seleniumPage;

    @Given("^open form selenium easy$")
    public void openFormSeleniumEasy() {
        seleniumPage.open();
    }

    @When("^user filling the following data$")
    public void userFillingTheFollowingData(DataTable formDataTable) {
        Map<String, Object> formData = formDataTable.asMap(String.class, Object.class);
        //get value by key
        seleniumPage.fillFirstName(formData.get("firstName").toString());
        seleniumPage.fillLastName(formData.get("lastName").toString());
        seleniumPage.fillEmail(formData.get("email").toString());
        seleniumPage.fillPhone(formData.get("phone").toString());
        seleniumPage.fillAddress(formData.get("address").toString());
        seleniumPage.fillCity(formData.get("city").toString());
        seleniumPage.chooseState(formData.get("state").toString());
        seleniumPage.fillZip(formData.get("zip").toString());
        seleniumPage.fillWebsite(formData.get("domain").toString());
        seleniumPage.chooseHosting(Boolean.valueOf(formData.get("hosting").toString()));
        seleniumPage.fillDesc(formData.get("description").toString());
        seleniumPage.clickBtnSubmit();

//        //contains key
//        System.out.println(formData.containsKey("firstName"));
//        System.out.println(formData.containsKey("lastName"));
//
//        //hashMap
//        Map<String, Object> map = new HashMap<>();
//        map.put("firstName", "Odelia");
//        map.put("lastName", "salsabila");
//        System.out.println(map);
    }

    @Then("^user should see filled form$")
    public void userShouldSeeFilledForm() {
        String actualUrl = seleniumPage.getDriver().getCurrentUrl();
        String expectedUrl = "https://www.seleniumeasy.com/test/input-form-demo.html1";
        assertThat("invalid url", actualUrl, equalTo(expectedUrl));
    }
}
