package com.training.future.batchfour.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class WikipediaPage extends PageObject {
    @FindBy(tagName = "body")
    private WebElementFacade bodyWikipedia;

    public String getTxtWikipedia() {
        return bodyWikipedia.getText();
    }
}
