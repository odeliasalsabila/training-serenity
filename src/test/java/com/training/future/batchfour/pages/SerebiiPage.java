package com.training.future.batchfour.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@DefaultUrl("https://www.serebii.net/fireredleafgreen/kantopokedex.shtml")
public class SerebiiPage extends PageObject {
    private static String POKEMON_PATH = "//table[@class='tab']//tbody//tr//td[@class='fooinfo'][3]//a[contains(text(),'${name}')]";
    private static String POKEMON_NO_PATH = "//td[@class='tooltabcon'][contains(text(),'${name}')]";

    public void findPokemon(String name) {
        WebElement pokemonName = getDriver().findElement(By.xpath(POKEMON_PATH.replace("$name", name)));
        pokemonName.click();
    }

    public String getPokemonNo(String name) {
        WebElementFacade pokemonName = find(By.xpath(POKEMON_NO_PATH.replace("$name", name)));
        return pokemonName.getText();
    }

    public String getPokemonName(String nameExpected) {
        return null;
    }
}
