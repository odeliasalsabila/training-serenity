package com.training.future.batchfour.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://duckduckgo.com/")
public class Duckduckgopage extends PageObject {
    @FindBy(name = "q")
    private WebElementFacade txtSearch;

    @FindBy(linkText = "https://id.wikipedia.org/wiki/Blibli.com")
    private WebElementFacade blibliWikipedia;

    public void  searching(String keyword) {
        txtSearch.typeAndEnter(keyword);
    }

    public void chooseBlibliWikipedia() {
        blibliWikipedia.click();
    }

}
