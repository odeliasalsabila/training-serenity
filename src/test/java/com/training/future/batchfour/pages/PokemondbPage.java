package com.training.future.batchfour.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://pokemondb.net/pokedex/game/red-blue-yellow")
public class PokemondbPage extends PageObject {
    @FindBy(xpath = "//a[@class='ent-name'][text()='Bulbasaur']")
    private WebElementFacade bulbasaurPokemon;

    @FindBy(xpath = "//h1[contains(text(),'Bulbasaur')]")
    private WebElementFacade pokemonName;

    @FindBy(xpath = "//strong[1]")
    private WebElementFacade pokemonNo;

    public WebElementFacade getNamePokemon(){
        return pokemonName;
    }

    public WebElementFacade getNoPokemon() {
        return pokemonNo;
    }

    public void clickPokemon() {
        bulbasaurPokemon.click();
    }
}
