package com.training.future.batchfour.pages;

import cucumber.api.java.eo.Se;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

@DefaultUrl("https://www.seleniumeasy.com/test/input-form-demo.html")
public class SeleniumPage extends PageObject {
    @FindBy(xpath = "//input[@placeholder='First Name']")
    private WebElementFacade firstName;

    @FindBy(xpath = "//input[@placeholder='Last Name']")
    private WebElementFacade lastName;

    @FindBy(xpath = "//input[@placeholder='E-Mail Address']")
    private WebElementFacade email;

    @FindBy(name = "phone")
    private WebElementFacade phone;

    @FindBy(name = "address")
    private WebElementFacade address;

    @FindBy(name = "city")
    private WebElementFacade city;

    @FindBy(name = "state")
    private WebElementFacade state;

    @FindBy(name = "zip")
    private WebElementFacade zip;

    @FindBy(name = "website")
    private WebElementFacade website;

    @FindBy(xpath = "//input[@name='hosting'][@value='no']")
    private WebElementFacade hostingNo;

    @FindBy(xpath = "//input[@name='hosting'][@value='yes']")
    private WebElementFacade hostingYes;

    @FindBy(xpath = "//textarea[@placeholder='Project Description']")
    private WebElementFacade projectDesc;

    @FindBy(xpath = "//span[@class='glyphicon glyphicon-send']")
    private WebElementFacade btnSubmit;

    public void fillFirstName(String name) {
        firstName.type(name);
    }

    public void fillLastName(String name) {
        lastName.type(name);
    }

    public void fillEmail(String emailTxt) {
        email.type(emailTxt);
    }

    public void fillPhone(String phoneTxt) {
        phone.type(phoneTxt);
    }

    public void fillAddress(String alamat) {
        address.type(alamat);
    }

    public void fillCity(String cityTxt) {
        city.type(cityTxt);
    }

    public void chooseState(String name) {
        Select option = new Select(state);
        option.selectByVisibleText(name);
    }

    public void fillZip(String zipTxt) {
        zip.type(zipTxt);
    }

    public void fillWebsite(String domain){
        website.type(domain);
    }

    public void chooseHosting(Boolean hosting) {
        if (hosting){
            hostingYes.click();
        }else {
            hostingNo.click();
        }
    }

    public void fillDesc(String description){
        projectDesc.type(description);
    }

    public void clickBtnSubmit() {
        btnSubmit.click();
    }
}
